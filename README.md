System do wyceny usług

Słownik
Klient- osoba, która chce skorzystać z usług
Usługodawca- osoba wyceniająca usługę

Historyjki użytkownika
Minimum Viabla Product:
1. Jako klient chcę posiadać możliwość wypełnienia formularza opisującego usługę.
2. Jako klient chcę aby zamówienie wyceny było powiązane z moim adresem email (bez
rejestracji).
3. Jako klient chcę mieć możliwość rejestracji.
4. Jako klient chcę mieć możliwość dodania zamówienia wyceny (bez rejestracji) do mojego
konta. Zamówienie wyceny zawiera podstawowe pola dotyczące wyceny oraz adres email usługodawcy.
5. Jako klient chcę widzieć moje zamówienia wyceny oraz ich status (złożona,
zaakceptowana, wyceniona, odrzucona (z przyczyną odrzucenia)), a także wycenę.
6. Jako usługodawca chcę mieć możliwość logowania się do widoku usługodawcy.
7. Jako usługodawca chcę zarządzać wszystkimi otrzymanymi zamówieniami wyceny.
8. Jako usługodawca chcę mieć pogrupowane zamówienia po statusach rezerwacji, osobno
każdy status. W przypadku niewycenionych posortowane od otrzymanego najwcześniej
(najstarszych). W przypadku pozostałych od najmłodszego.
9. Jako usługodawca chcę mieć możliwość akceptacji zamówienia (tzn. potwierdzenia, że
zostanie wycenione).
10. Jako usługodawca chcę mieć możliwość odrzucenia wyceny zamówienia(konieczne
podanie przyczyny).
11. Jako usługodawca chcę mieć możliwość wyceny zamówienia.
Nice to have:
12. Jako klient chcę otrzymywać powiadomienie o każdej zmianie mojego zamówienia
wyceny.
13. Jako klient chcę otrzymywać wycenę zamówienia na maila.
14. Jako usługodawca chcę otrzymywać powiadomienia o otrzymaniu zamówienia wyceny na
adres email.